# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 22:41:57 2020

@author: hp
"""
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 10:28:50 2020

@author: xiang_yaobing
"""

import os 
import random
from PIL import Image
from tensorflow.keras.models import Sequential,load_model
from tensorflow.keras.layers import LSTM,Dense,Activation,SimpleRNN,Conv2D,MaxPool2D,Flatten,Reshape,Dropout
from tensorflow.keras.callbacks import EarlyStopping,ModelCheckpoint
from tensorflow.keras.metrics import categorical_accuracy
from tensorflow.keras.optimizers import RMSprop
import numpy as np
from tensorflow.keras import utils
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import cv2

def load_data(path1, path2):
#读入文件列表，path代表2个类别的文件路径
    filelist1 = [os.path.join(path1, f) for f in os.listdir(path1)]
    filelist2 = [os.path.join(path2, f) for f in os.listdir(path2)]
    x_test = []
    y_test = []
    n1 = len(filelist1)
    n2 = len(filelist2)
    for img in filelist1:
        im = np.array(cv2.imread(img,cv2.IMREAD_UNCHANGED))#读图像65535
#        m = im.copy()
#        im-im.min()
        im = np.array((im-im.min())/im.max())#im = np.array(np.loadtxt(img))
        #im = im.flatten()
        x_test.append(im)
    for img in filelist2:
        im = np.array(cv2.imread(img,cv2.IMREAD_UNCHANGED))
        im = np.array((im-im.min())/im.max())
        #print(img)
        #im = np.array(np.loadtxt(img))
        #im = im.flatten()
        x_test.append(im)
    #x_test = image_standerization(np.array(x_test))#数据标准化
    #自己造标签 总共2类，所以标签是01
    y_test = np.zeros((n1 + n2), dtype=int)
    for i in range(n1): 
        y_test[i] = 0#恒星\噪声
    for i in range(n2):
        y_test[n1 + i] = 1#目标
    return np.array(x_test), y_test

nb_classes = 2
path1 = '..\\candidateRegion\\0\\'#恒星/噪声路径
path2 = '..\\candidateRegion\\1\\'#目标路径

s1Path = '..\\model\\simulate_best_weights_v1.0.h5'#单真实数据
s2Path = '..\\model\\s2_best_weights_v1.0.h5'


singlePath = '..\\model\\single_best_weights_v1.0.h5'
data, labels = load_data(path1, path2)
data1, labels1 = load_data('..\\testset\\noClusterKDEpicture\\no_cluster\\loc\\', '..\\testset\\ClusterKDEpicture\\cluster\\loc\\')
data1= np.reshape(data1, (data1.shape[0],60,60,1))
#y_test = utils.to_categorical(y_test)

modelS1 = load_model(s1Path)
modelS2 = load_model(s2Path)


#modelSingle = load_model(singlePath)
#%%
data, labels = load_data(path1, path2)
S1Result = modelS1.predict_classes(data)
S2Result = modelS2.predict_classes(data)
#SingleResult = modelSingle.predict_classes(data)