# -*- coding: utf-8 -*-
"""
Created on Thu May 30 20:02:35 2019

@author: xiang_yaobing

代码用于获取gaia数据
输入数据，团表
输出数据,团gaia dr2数据，格式csv

"""
from astropy.io import fits
import astropy.units as u
from astropy.coordinates import SkyCoord
from astroquery.gaia import Gaia

list_path = 'E:\\天文\\团 list\\Gaia.fits'
def get_csv(list_path):
    data=fits.open(list_path) 
    dat = data[1].data
    for i in range(len(dat)):
        name = dat[i][0]
        ra = dat[i][1]
        dec = dat[i][2]
        coord = SkyCoord(ra, dec, unit=(u.degree, u.degree), frame='icrs')
        radius = u.Quantity(0.1, u.deg)
        Gaia.cone_search_async(coord,radius,output_file=name,output_format='csv',verbose = True,dump_to_file = True)

get_csv(list_path)