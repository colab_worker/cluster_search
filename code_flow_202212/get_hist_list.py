# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 19:30:17 2019

@author: xiang_yaobing
"""
import numpy as np


def get_list(data, bins = 100):#获取每个区间有多少个数据点
    '''
    step：步长
    bins：划分格数
    data：每个点7邻近点，平均距离计算数据
    hist_list：格网坐标和频数
    '''
    begin = np.min(data)
    end = np.max(data)
    step = (end-begin)/bins
    hist_list = np.zeros([bins,2])
    for i in range(bins):
        hist_list[i][0] = begin+i*step
        filt1 = data[data>(begin+i*step)]
        hist_list_num = np.size(filt1[filt1<(begin+(i+1)*step)])
        hist_list[i][1] = hist_list_num
    return hist_list

def find_split_point_way1(hist_list, bins = 100):#找条形图切分点，通过趋势寻找
    for i in range(bins):
        if hist_list[i][1]>hist_list[i+1][1]:
            if hist_list[i+1][1]<hist_list[i+2][1]:
                if hist_list[i+2][1]<hist_list[i+3][1]:
                    return (hist_list[i+2][0]+hist_list[i+3][0])/2
        if i > bins/2:
            print('error, do not find the split point!')
            
def find_split_point_way2(hist_list, bins = 100):#找条形图切分点，通过区域最小值寻找
    min_num = hist_list[int(bins/25+1)][1]#取值范围为根据论文所取经验值
    min_num_list = np.zeros([int(bins/3), 2])
    k = 0
    for i in range(int(bins/25+1), int(bins/4), 1):
        if hist_list[i+1][1] < min_num:
            min_num = hist_list[i+1][1]
            min_num_list[k][0] = hist_list[i+1][0]
            min_num_list[k][1] = hist_list[i+1][1]
            k = k+1
        else:
            min_num_list[k][0] = hist_list[i][0]
            min_num_list[k][1] = hist_list[i][1]
            k = k+1
    min_num_list = min_num_list[min_num_list[:,0]< 0.3]#强制限定距离值小于0.3，经验值
    value_min_list = min_num_list[min_num_list[:,1] != 0] 
             
    #min_least = np.min(value_min_list[:,1])
    loc = np.where(value_min_list[:, 1] ==  np.min(value_min_list[:,1]))
    split_value = value_min_list[loc, 0]
    #split_point = min_num_list[min_num_list[:,1] == min_least]
    #value = float(split_point[0][0])
    return split_value
    
        
            
            
            