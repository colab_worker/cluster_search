# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 23:36:43 2019

@author: xiang_yaobing
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 08:45:33 2018

@author: Lenovo
"""
import os 
from PIL import Image
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM,Dense,Activation,SimpleRNN,Conv2D,MaxPool2D,Flatten,Reshape,Dropout
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.metrics import categorical_accuracy
from tensorflow.keras.optimizers import RMSprop
import numpy as np
from tensorflow.keras import utils
from sklearn.model_selection import train_test_split

def load_data(path1, path2):
#读入文件列表，path代表2个类别的文件路径
    filelist1 = [os.path.join(path1, f) for f in os.listdir(path1)]
    filelist2 = [os.path.join(path2, f) for f in os.listdir(path2)]
    x_test = []
    y_test = []
    n1 = len(filelist1)
    n2 = len(filelist2)
    for img in filelist1:
        im = np.array(Image.open(img))#读图像
        #im = np.array(np.loadtxt(img))
        #im = im.flatten()
        x_test.append(im)
    for img in filelist2:
        im =np.array(Image.open(img))
        #print(img)
        #im = np.array(np.loadtxt(img))
        #im = im.flatten()
        x_test.append(im)
    x_test = np.array(x_test)
    #自己造标签 总共2类，所以标签是01
    y_test = np.zeros((n1 + n2), dtype=int)
    for i in range(n1): 
        y_test[i] = 0
    for i in range(n2):
        y_test[n1 + i] = 1
    return x_test, y_test

nb_classes = 2
path1 = '..\\trainset\\noClusterKDEpicture_multiKde\\'
path2 = '..\\trainset\\ClusterKDEpicture_multiKde\\'
data, labels = load_data(path1, path2)
x_train, x_test, y_train, y_test = train_test_split(data/255, labels, test_size=0.1)
y_train = utils.to_categorical(y_train)
y_test = utils.to_categorical(y_test)


model = Sequential()
# 第一个卷积层，32个卷积核，大小５x5，卷积模式SAME,激活函数relu,输入张量的大小
model.add(Conv2D(filters= 16, kernel_size=(5,5), padding='Same', activation='relu',input_shape=(60,60,3)))
model.add(Conv2D(filters= 16, kernel_size=(3,3), padding='Same', activation='relu'))
# 池化层,池化核大小２x2
model.add(MaxPool2D(pool_size=(2,2)))

model.add(Conv2D(filters= 32, kernel_size=(3,3), padding='Same', activation='relu'))
model.add(Conv2D(filters= 32, kernel_size=(3,3), padding='Same', activation='relu'))
model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))

model.add(Conv2D(filters= 64, kernel_size=(3,3), padding='Same', activation='relu'))
model.add(MaxPool2D(pool_size=(2,2), strides=(2,2)))

# 全连接层,展开操作，
model.add(Flatten())
# 添加隐藏层神经元的数量和激活函数
model.add(Dense(64, activation='relu'))    
model.add(Dropout(0.5))
model.add(Dense(32, activation='relu'))    
model.add(Dropout(0.5))
# 输出层
model.add(Dense(2, activation='softmax'))  

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())

model.fit(x_train, y_train, validation_split=0.1, verbose=1, batch_size=30, epochs=50)
model.save('cnn_model_loc_v1_multiKde.h5')