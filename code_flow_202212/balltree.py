# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 14:14:47 2019

@author: xiang_yaobing
"""
#测试 BallTree
from sklearn.neighbors import BallTree
import numpy as np
import get_hist_list as ghl
import matplotlib.pyplot as plt
import data_genrate_1 as dg

def get_data_split_config(mesure_data,bins=100):
    bt = BallTree(mesure_data,leaf_size=30, p=2)#球树最近邻法，欧式距离
    result = bt.query(mesure_data, k=8)[0]
    result_sum = np.sum(result,axis = 1)/7#确定平均距离
    result_sum_filt = result_sum[result_sum<1.2]
    data_list = np.zeros([np.size(result_sum_filt), 3])
    data_list[:, 0] = result_sum_filt
    hist_list = ghl.get_list(result_sum_filt, bins)
    split_point = ghl.find_split_point_way2(hist_list, bins)
    return split_point


def example_split_point():
    '''
    注意此处在处理1.fits文件时报错
    报错信息：return umr_minimum(a, axis, None, out, keepdims)
    ValueError: zero-size array to reduction operation minimum which has no identity
    初步判断是寻找分割点出现故障，留后待查
    '''
    bins=100
    data = dg.get_fits_to_array('E:\\天文\\cluster_data\\cluster_normal\\berkeley10.r0.2.fits')
    X = data[:,[0,1,2,3,5]]
    bt = BallTree(X,leaf_size=30, p=2)#球树最近邻法，欧式距离
    #bt = BallTree(X,leaf_size=30,="euclidean")
    result = bt.query(X, k=8)[0]
    result_sum = np.sum(result,axis = 1)/7#确定平均距离
    result_sum_filt = result_sum[result_sum<3]
    data_list = np.zeros([np.size(result_sum_filt), 3])
    data_list[:, 0] = result_sum_filt

    #plt.figure(figsize=(50, 30))
    hist_list = ghl.get_list(result_sum_filt, bins)
    split_point = ghl.find_split_point_way2(hist_list, bins)[0][0]
    #print('hello ', split_point)
    plt.hist(result_sum_filt, bins, color = 'g',alpha=0.5)
    plt.vlines(split_point, 0, np.max(hist_list[:,1]), colors = "r", linestyles = "dashed")
    
    plt.show()

    #plt.plot(hist_list[:,0], hist_list[:,1], c = 'b')
    plt.show()
    
def seaborn_plot(result_sum_filt):
    import seaborn as sns 
    sns.set_palette("hls") #设置所有图的颜色，使用hls色彩空间
    sns.distplot(result_sum_filt, color="b", bins=100, kde=True)