# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 10:41:02 2020

@author: xiang_yaobing
"""

#%%
import cv2
import matplotlib.pyplot as plt
import numpy as np
import random
import os
from PIL import Image

#im = np.array(Image.open("NoiseIM1.tif"))

def show(image):
    plt.imshow(image)
    plt.axis('off')
    plt.show()
name_ = ['IM_add']#,'IM6','IM7','IM8'

for name1 in name_:
    path1 = '..\\j\\'
    save_path = '..\\jpg\\'
    filelist1 = [os.path.join(path1, f) for f in os.listdir(path1)]
    filelist_name = os.listdir(path1)
    j = 0
    for i in filelist1:
        name = filelist_name[j].split('.')[0]
        image1=cv2.imread(i,cv2.IMREAD_UNCHANGED)
        image1 = np.array(image1)#+np.random.normal(size=(33,33))*10+313#添加背景为313的标准差为10的随机噪声
    #    a = image1.copy()
    #    b = image1.copy()
    #    c = image1
    #    a[image1>0] = 1
    #    b[image1==0] = 1
    #    b[image1>0] = 0
    #    
    #    image1 = image1 + a*313 + np.uint16(b*np.random.normal(313,10,(33,33)))
        show(image1)
        cv2.imwrite(save_path + name + '_orl_'+name1+'.tif', image1)
        image=cv2.flip(image1,1)#这里用到的是水平翻转，因为后面的参数是一
        show(image)
        cv2.imwrite(save_path +name + '_hf_'+name1+'.tif', image)
        image=cv2.flip(image1,0)#这里用到的是垂直翻转，因为后面的参数是0
        show(image)
        cv2.imwrite(save_path +name + '_vf_'+name1+'.tif', image)
        image=cv2.flip(image1,-1)#这里用到的是水平垂直翻转，因为后面的参数是-1
        show(image)
        cv2.imwrite(save_path +name + '_vhf_'+name1+'.tif', image)
        rows,cols,channel = image1.shape
        # 平移矩阵M：[[1,0,x],[0,1,y]]
        #random.seed(i)
        for n in range(21):
            
            M = np.float32([[1,0,random.randint(-15,15)],[0,1,random.randint(-15,15)]])
            dst = cv2.warpAffine(image1,M,(cols,rows))
            show(dst)
            cv2.imwrite(save_path +name +str(n)+ '_shift.tif', dst)
        j = j+1